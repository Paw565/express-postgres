import express from "express";
import db from "./db/migrate.js";
import getRedisClient from "./db/redisConnection.js";
import { user } from "./db/schema.js";

const app = express();

app.use(express.json());

const redis = getRedisClient();
const port = process.env.PORT || 3000;

app.get("/message", async (_, res) => {
  const message = await redis.get("message");
  res.json({ message });
});

app.post("/message", async (req, res) => {
  const { content } = req.body;

  if (!content) return res.status(400).json({ error: "Content is required" });

  const messages = await redis.set("message", content);
  res.status(201).json(messages);
});

app.get("/users", async (_, res) => {
  const result = await db.select().from(user);
  res.json(result);
});

app.post("/users", async (req, res) => {
  const { name, email, password } = req.body;
  const result = await db
    .insert(user)
    .values({ name, email, password })
    .returning();
  res.status(201).json(result.at(0));
});

app.listen(port, async () => {
  await redis.connect();
  console.log(`Server is running on port ${port}`);
});
