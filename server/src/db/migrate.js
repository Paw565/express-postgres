import { drizzle } from "drizzle-orm/postgres-js";
import { migrate } from "drizzle-orm/postgres-js/migrator";
import postgres from "postgres";

const connectionString = process.env.POSTGRES_CONNECTION_STRING;

// for migrations
const migrationClient = postgres(connectionString, { max: 1 });
migrate(drizzle(migrationClient), { migrationsFolder: "drizzle" });

// for query purposes
const queryClient = postgres(connectionString);
const db = drizzle(queryClient);

export default db;
